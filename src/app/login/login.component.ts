import { Component, OnInit } from '@angular/core';
import {EmployeeService} from "../employee.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private service: EmployeeService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  login(user: string, password: string) {
    this.service.postLoginData(user, password).subscribe(resp => {
      if (resp != undefined) {
        sessionStorage.setItem('bearer-token', (resp as any).access_token);
        this.service.setBearer();
        this.router.navigateByUrl('/dashboard');
      } else {
        alert("Wrong password or username. Please check and try again.");
      }
    });
  }

}
