import { Component, OnInit } from '@angular/core';
import {Employee} from "../Employee";
import {ActivatedRoute} from "@angular/router";
import {EmployeeService} from "../employee.service";
import {Location} from "@angular/common";

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {

  employee: Employee | undefined;
  bearer= this.service.getBearer();

  constructor(
    private route: ActivatedRoute,
    private service: EmployeeService,
    private location: Location
  ) { }

  ngOnInit(): void {
    if(this.bearer) this.getEmployee();
  }

  getEmployee(): void {
    const id = parseInt(this.route.snapshot.paramMap.get('id')!, 10);
    this.service.getEmployee(id).subscribe(employee => this.employee = employee);
  }

  deleteEmployee(employee: Employee): void {
    let id = employee.id;
    if (id == undefined) {
      alert(`Employee with id = ${id} not found.`);
    } else {
      this.service.deleteEmployee(id).subscribe()
      window.location.href ="/employees";
    }
  }

  logout(): void {
    sessionStorage.removeItem('bearer-token');
    window.location.href = '/';
  }

  goBack(): void {
    this.location.back();
  }

}
