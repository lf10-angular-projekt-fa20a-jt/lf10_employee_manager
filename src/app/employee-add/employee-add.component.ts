import { Component, OnInit } from '@angular/core';
import {Employee} from "../Employee";
import {ActivatedRoute} from "@angular/router";
import {EmployeeService} from "../employee.service";
import {Location} from "@angular/common";

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.css']
})

export class EmployeeAddComponent implements OnInit {

  employees: Employee[] = [];
  bearer= this.service.getBearer();

  constructor(
    private route: ActivatedRoute,
    private service: EmployeeService,
    private location: Location
  ) { }

  ngOnInit(): void {
    if(this.bearer) this.getEmployees();
  }

  getEmployees(): void {
    this.service.getEmployees().subscribe(employees => this.employees = employees);
  }

  goBack(): void {
    this.location.back();
  }

  add(
    lastName: string,
    firstName: string,
    street: string,
    postcode: string,
    city: string,
    phone: string,
  ): void {
    lastName = lastName.trim();
    firstName = firstName.trim();
    street = street.trim();
    postcode = postcode.trim();
    city = city.trim();
    phone = phone.trim();

    if (!lastName || !firstName || !street || !postcode || !city || !phone || postcode.length != 5) { return;}
    this.service.addEmployee({lastName, firstName, street, postcode, city, phone} as Employee).subscribe(employee => {this.employees.push(employee);this.goBack()});
  }

  logout(): void {
    sessionStorage.removeItem('bearer-token');
    window.location.href = '/';
  }
}





