import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {EmployeesComponent} from "./employees/employees.component";
import {EmployeeDetailComponent} from "./employee-detail/employee-detail.component";
import {EmployeeAddComponent} from "./employee-add/employee-add.component";
import {EmployeeEditComponent} from "./employee-edit/employee-edit.component";
import {LoginComponent} from "./login/login.component";
import {EmployeeSearchComponent} from "./employee-search/employee-search.component";
import {DashboardComponent} from "./dashboard/dashboard.component";

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'employees', component: EmployeesComponent},
  {path: 'detail/:id', component: EmployeeDetailComponent},
  {path: 'employee/add', component: EmployeeAddComponent},
  {path: 'employee/edit/:id', component: EmployeeEditComponent},
  {path: 'search', component: EmployeeSearchComponent},
  {path: 'dashboard', component: DashboardComponent}
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
