import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, of} from "rxjs";
import {Employee} from "./Employee";

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private employeesUrl = '/backend';
  private bearer = sessionStorage.getItem('bearer-token');

  private httpOptions = {
    headers: new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${this.bearer}`)
  };

  private loginHttpOptions = {
    headers: new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded')
  };

  constructor(private http: HttpClient) { }

  /** GET employees from the backend */
  getEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.employeesUrl, this.httpOptions);
  }

  /** GET employee by id. Returns 404 if id not found. */
  getEmployee(id: number): Observable<Employee> {
    const url = `${this.employeesUrl}/${id}`;
    return this.http.get<Employee>(url, this.httpOptions).pipe(
      catchError(this.handleError<Employee>(`get Employee with id ${id}`))
    );
  }

  /** POST new employee. Returns 404 if id not found. */
  addEmployee(employee: Employee): Observable<Employee> {
    return this.http.post<Employee>(this.employeesUrl, employee, this.httpOptions);
  }

  /** PUT new employee. Returns 404 if id not found. */
  editEmployee(employee: Employee): Observable<Employee> {
    const url = `${this.employeesUrl}/${employee.id}`;
    return this.http.put(url, employee, this.httpOptions);
  }

  /** POST login info to get bearer token. */
  postLoginData(user: string, password: string): Observable<string> {
    const url = 'http://authproxy.szut.dev';
    let body = `grant_type=password&client_id=employee-management-service&username=${user}&password=${password}`;
    return this.http.post<string>(url, body, this.loginHttpOptions).pipe(
      catchError(this.handleError<string>('login'))
    );
  }

  /** DELETE employee by id. */
  deleteEmployee(id: number): Observable<Employee> {
    const url = `${this.employeesUrl}/${id}`;
    return this.http.delete(url, this.httpOptions).pipe(
      catchError(this.handleError<Employee>(`deleting employee with id = ${id}`))
    );
  }

  getBearer(){
    return this.bearer;
  }

  setBearer(): void {
    this.bearer = sessionStorage.getItem('bearer-token');
    this.httpOptions = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer}`)
    };
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    }
  }

}
