import { Component, OnInit } from '@angular/core';
import {Employee} from "../Employee";
import {EmployeeService} from "../employee.service";
import {Location} from "@angular/common";

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  employees: Employee[] = [];
  bearer= this.service.getBearer();
  constructor(private service: EmployeeService, private location: Location) { }

  ngOnInit(): void {
    if(this.bearer) this.getEmployees();
  }

  getEmployees(): void {
    this.service.getEmployees().subscribe(employees => this.employees = employees);
  }

  deleteEmployee(employee: Employee): void {
    this.employees = this.employees.filter(e => e !== employee);
    let id = employee.id;
    if (id == undefined) {
      alert(`Employee with id = ${id} not found.`);
    } else {
      this.service.deleteEmployee(id).subscribe();
    }
  }

  logout(): void {
    sessionStorage.removeItem('bearer-token');
    window.location.href = '/';
  }

  goBack(): void {
    this.location.back();
  }

}
