import { Component, OnInit } from '@angular/core';
import {EmployeeService} from "../employee.service";
import {Employee} from "../Employee";

@Component({
  selector: 'app-employee-search',
  templateUrl: './employee-search.component.html',
  styleUrls: ['./employee-search.component.css']
})
export class EmployeeSearchComponent implements OnInit {

  employees: Employee[] = [];
  result: Employee[] = [];
  bearer= this.service.getBearer();

  constructor(private service: EmployeeService) { }

  ngOnInit(): void {
    if(this.bearer) this.service.getEmployees().subscribe(employees => this.employees = employees);
  }

  search(term: string): void {
    this.result = [];
    for (let employee of this.employees) {
      let fullname = employee.firstName + " " + employee.lastName;
      if (term !== "" && fullname.includes(term)) {
        this.result.push(employee);
      }
    }
  }

  logout(): void {
    sessionStorage.removeItem('bearer-token');
    window.location.href = '/';
  }

}
