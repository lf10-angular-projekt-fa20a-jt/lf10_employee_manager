import { Component, OnInit } from '@angular/core';
import {EmployeeService} from "../employee.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  bearer = this.service.getBearer();

  constructor(private service: EmployeeService) { }

  ngOnInit(): void {
  }

  logout(): void {
    sessionStorage.removeItem('bearer-token');
    window.location.href = '/';
  }

}
